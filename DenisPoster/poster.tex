\include{header}

\title{\Huge {Morse-Witten theory and its applications}}
\author{\Large{J.~Winkelmann$^{a}$, F.F.~Dunne$^{a}$, \underline{D.~Weaire}$^{a}$ R.~Höhler$^{b}$ and S.~Hutzler$^{a}$}}
\subtitle{
\Large{$^{a}$School of Physics, Trinity College Dublin, The University of Dublin, Ireland\\
$^{b}$Sorbonne Universités, UPMC Université Paris 06, Institut des NanoSciences de Paris, France
}}
\date{}

\titlegraphic{%
  \includegraphics[clip=true, trim= 10 10 0 10,width=1.\linewidth]{Abbildungen/trinity-stacked.jpg}
}

\institute{%
 \includegraphics[width=\linewidth]{Abbildungen/foamarms.png}%
}

\begin{document}

\begin{columns}[T]
\column{0.8\textwidth}
\vspace{-1cm}
\begin{block}{Abstract taken from Morse and Witten (1993):}
We discuss the deformation of a fluid droplet in an emulsion under external forces, such as those exerted by contact with neighbouring droplets.
We find that the deformation energy associated with a small droplet-droplet contact scales as \textcolor{tugreen}{$f^2\ln(1 / f)$} with the force $f$ exerted between droplets.
We consider the equation of state of an emulsion in which droplets are assumed to interact only via such contact forces, and obtain an osmotic compressibility which diverges logarithmically with the osmotic pressure in the limit of small pressure.

\vspace{0.25cm}
\large{D.~C. Morse and T.~A. Witten. \emph{Droplet elasticity in weakly compressed emulsions.} Europhys. Lett.,\textbf{22}, 549--555, (1993).}

\Large{\textcolor{tugreen}{
These important results have been poorly appreciated and hardly ever applied!
\begin{itemize}
\item
Logarithmic form of interaction in 3D
\item
Extended system of bubbles or drop can be represented by a force network: see below
\end{itemize}
}}

\end{block}
\column{0.23\textwidth}
\centering
\begin{figure}
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\centering
\includegraphics[width=0.95\textwidth]{Abbildungen/MorseWittenDrop.png}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\node at (0.51, 0.45) {\LARGE{DIVERGENCE!}};
\end{scope}
\end{tikzpicture}
\caption{Sessile drop.}
\end{figure}
\end{columns}

% Morse--Witten Theory and simulation %%%%%%%%%%%%%%%%%%%%%%%%%
\begin{block}{The Morse--Witten (MW) theory for 2D bubbles}
\begin{columns}
\column{0.28\textwidth}
\centering
\begin{figure}
\includegraphics[width=0.8\textwidth]{Abbildungen/bubbleDescr.pdf}
\caption{Deformation of a 2D bubble with point force.}
\end{figure}
\begin{equation}
r(\theta) = R_0 (1 + \delta r(\theta))
\end{equation}
\column{0.48\textwidth}
\centering
\begin{tcolorbox}[
colback=tulight,
boxrule=5pt,
colframe=blue,
arc=5mm,
]
What is the shape $\delta r(\theta)$ of a \textcolor{blue}{single, incompressible 2D bubble}, subject to a single contact force $f$ (and a compensating body force)?
\end{tcolorbox}

\begin{tcolorbox}[
colback=white,
boxrule=1pt,
colframe=black,
arc=5mm,
]
\textbf{Linear differential equation} for the $\delta r(\theta)$:
\begin{equation*}
-\left( \frac{\mathrm{d}^2}{\mathrm{d}\theta^2} \right) \delta r(\theta) = A + \frac{f}{\pi} \cos\theta
\end{equation*}
\end{tcolorbox}

\begin{tcolorbox}[
colback=white,
boxrule=1pt,
colframe=black,
arc=5mm,
]
\textbf{Solution} for $\delta r(\theta)$:
\begin{equation*}
\delta r(\theta) = \frac{f}{2 \pi} g(\theta)\,, \quad g(\theta) = (\pi - \theta) \sin\theta - \frac{\cos\theta}{2} - 1
\end{equation*}
\end{tcolorbox}
\begin{itemize}
\item
$\delta r$ \textcolor{blue}{linear} in $f$
\item
Area $A = \pi R_0^2$ is conserved
\item
Change in area of the enclosed curve is of higher order $\mathcal{O}(f^3)$
\end{itemize}
\column{0.26\textwidth}
\centering
\begin{figure}
\includegraphics[width=0.75\textwidth]{Abbildungen/Fig2a.pdf}

\includegraphics[width=0.75\textwidth]{Abbildungen/Fig2b.pdf}
\end{figure}

\end{columns}
\vspace*{-2.8cm}
\large{
D.~Weaire, R.~Höhler, and S.~Hutzler. \emph{Bubble-bubble interactions in a 2d foam, close to the wet limit.}Adv. Colloid Interface Sci, accepted, (2017). \\
\url{https://dx.doi.org/10.1016/j.cis.2017.07.004}.}
\end{block}
%
\vspace{-0.5cm}
% 2D Applications of MW theoy %%%%%%%%%%%%%%%%%%%%%
\begin{columns}[T]
\column{0.5\textwidth}
\begin{block}{MW vs. exact: Bubble between plates}
\begin{figure}
\hspace*{-4cm}
\begin{subfigure}{0.47\textwidth}
\centering
\includegraphics[width=\textwidth]{Abbildungen/{betweenPlate0.10}.pdf}
\caption{\Large Deformation of $\SI{10}{\percent}$.}
\end{subfigure}
\begin{subfigure}{0.47\textwidth}
\centering
\includegraphics[width=\textwidth]{Abbildungen/{betweenPlate0.20}.pdf}
\caption{\Large Deformation of $\SI{20}{\percent}$.}
\end{subfigure}
\hspace*{-6cm}
\end{figure}
\end{block}

\column{0.5\textwidth}
\begin{block}{MW applied to 2D disordered foam}
\centering
\begin{figure}
\begin{subfigure}{0.32\textwidth}
\centering
\includegraphics[width=\textwidth]{Abbildungen/MW_diagram.pdf}
\caption{\Large Bubble with multiple contact forces.}
\end{subfigure}
\begin{subfigure}{0.32\textwidth}
\centering
\includegraphics[width=\textwidth, clip=true, trim=120 40 105 40]{Abbildungen/MWSamplePhi0_9.png}
\caption{\Large 2D foam at $\phi = 0.9$.}
\end{subfigure}
\begin{subfigure}{0.32\textwidth}
\centering
\includegraphics[width=\textwidth, clip=true, trim=120 40 105 40]{Abbildungen/mw_network_09.png}
\caption{\Large Force network.}
\end{subfigure}

\end{figure}
\end{block}
\end{columns}

% 3D Applications of 3D ordered foam %%%%%%%%%%%%%%%%%%
\begin{columns}
\column{0.4\textwidth}
\begin{block}{MW applied to 3D foam}
\begin{figure}
\captionsetup{format=hang}
\centering
\begin{subfigure}{0.47\textwidth}
\centering
\includegraphics[width=0.75\textwidth]{Abbildungen/HoehlerPacking.png}
\caption{\Large{Disordered foam.}}
\end{subfigure}
\begin{subfigure}{0.47\textwidth}
\centering
\includegraphics[width=0.75\textwidth]{Abbildungen/HoehlerPackingStress.png}
\caption{\Large{Ordered foam.}}
\end{subfigure}
%\caption{3D ordered foam in equilibrium (left) and deformed by uniaxial strain (right), modelled with MW theory.}
\end{figure}
\begin{itemize}
\item
Analysis of Surface Evolver simulations of foams reveals non-pairwise interaction between bubbles
\end{itemize}
\large{
R.~Hoehler and S.~Cohen-Addad. \emph{Many-body interactions in soft jammed materials.} Soft Matter \textbf{13}, 1371-1383, (2017).
}
\end{block}

% The pendant drop %%%%%%%%%%%%%%%%%%%%
\column{0.6\textwidth}
\begin{block}{The sessile and pendant drop}
\vspace{-1cm}
\begin{figure}
\includegraphics[clip=true, trim=105 0 90 20, width=0.6\textwidth]{Abbildungen/pic.pdf}
\caption{The sessile drop (left) and pendant drop (right): proposed measurements}
\end{figure}
\vspace{-0.5cm}
\begin{tcolorbox}[
colback=white,
boxrule=5pt,
colframe=tugreen,
arc=5mm,
]
\textbf{Surface tension $\sigma$:}
\vspace{-0.3cm}
\begin{columns}[T]
\column{0.6\textwidth}
\begin{equation*}
\sigma = \frac{\ln2}{24} \Delta\rho g \frac{(L_x + L_y)^3}{|L_x - L_y|}
\end{equation*}
\column{0.4\textwidth}
\textbf{Simple} formula, needing only 2 length measurements.
\end{columns}
\end{tcolorbox}
\large{
S.~Hutzler, J.~Ryan-Purcell, D. Weaire, \emph{A simple formula for the estimation of surface tension
from two length measurements for a pendant or sessile drop}, in preparation, (2017).
}
\end{block}
\vspace{-0.6cm}
\end{columns}

\vspace{0.5cm}
\begin{tcolorbox}[
colback=white,
boxrule=5pt,
colframe=tugreen,
arc=5mm,
]
\centering
\huge{\textcolor{tugreen}{A review on this subject is in preparation (R. Höhler, D. Weaire).}}
\end{tcolorbox}

\vspace{-1cm}
%%%%%%%%%%%%%%%%%% REFERENCES %%%%%%%%%%%%%%%%%%%%%%%%
  \vspace*{\fill}
  \begin{columns}[b]
  \column{0.6\textwidth}
  \begin{block}[fonttitle=\normalsize, arc=10mm]{References}
    \begin{multicols}{3}
      \nocite{*}\footnotesize%
      \printbibliography%
    \end{multicols}
  \end{block}
  \vspace{-1cm}
  \column{0.4\textwidth}
  \centering
  \begin{columns}
  \column{0.5\linewidth}
  \includegraphics[width=\linewidth]{Abbildungen/sfi_logo}
  \column{0.5\linewidth}
  \includegraphics[width=\linewidth, clip=true, trim=325 0 0 0]{Abbildungen/Longlogo.jpg}
  \vspace{-1cm}
  \end{columns}
\end{columns}  

\end{document}
