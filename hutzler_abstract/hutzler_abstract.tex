\documentclass{erlangen}
\usepackage{hyperref}
\usepackage{tikz}
\begin{document}

\title{Bubble-bubble interactions in 2D, just above the jamming transition}

\author{
 \underline{S. Hutzler}
}
\correspondingauthor{stefan.hutzler@tcd.ie}
\affiliation{School of Physics, Trinity College Dublin, The University of Dublin, Ireland}
\author{D. Weaire}
\affiliation{School of Physics, Trinity College Dublin, The University of Dublin, Ireland}
\author{F.F. Dunne}
\affiliation{School of Physics, Trinity College Dublin, The University of Dublin, Ireland}
\author{J. Winkelmann}
\affiliation{School of Physics, Trinity College Dublin, The University of Dublin, Ireland}
\author{R. H\"ohler}
\affiliation{Institut des NanoSciences de Paris,
 Universit\'e Pierre \& Marie Curie (Paris 6), France \\}
 
 \affiliation{Universit\'e Paris-Est Marne-la-Vall\'ee, 5 Bd Descartes, Champs-sur-Marne, F-77454 Marne-la-Vall\'ee cedex 2, France.}

\maketitle

Two-dimensional wet foams or weekly
compressed emulsions 
are often modelled
as circular disks whose overlap is resisted by a force that varies linearly
with distance. Despite the insight that this so-called
bubble model \cite{Durian95} has
provided for foam rheology (when augmented by a viscous dissipation term)
\cite{Durian97,Langlois08},
it is unrealistic in various respects. In particular Morse and Witten
\cite{MorseWitten}
have 
shown that drops/bubbles just above the jamming transition do not obey simple pairwise force laws. 
On
the other hand, numerical simulations based on balancing surface tension
and pressure force (2D software \texttt{Plat} \cite{Bolton91, Bolton92}) or minimising interface energy
(Surface Evolver \cite{brakke1992}), which work well for dry foams, have limited success for 
wet foams.

Here we present the development of Morse and Witten's approach to relative 
forces and positions for wet foams in a scheme analogous to that of H\"ohler 
and Cohen-Addad in 3D \cite{HoehlerAddad}.
We hope to clarify the discrepancy
that has recently emerged in the variation of average contact number of
bubbles with liquid fraction, namely a square root variation in the
bubble model, and a linear relation in \texttt{Plat} simulations which consider adjustments of bubble shapes \cite{Winkelmann16}.
%We also comment on the possibilities for developing a meaningful dynamical model
based on the Morse-Witten formalism.

The Morse-Witten force law deserves to take its place as one of 
the canonical laws of
soft matter, but has hitherto been poorly appreciated, largely on the
account of the difficulty of interpreting the original paper.
Our re-formulation of the theory, initially in 2d, should offer
clarification.

\begin{figure}[h!]
\centering
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[clip=true, trim=70 0 70 0, width=0.3\linewidth]{{SampleN25phi0.8800}.pdf}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
  \node at (0.1, 1.05) {(a)};
\end{scope}
\end{tikzpicture}
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image2) at (0,0) {
\includegraphics[clip=true, trim=60 170 60 200, width=0.3\linewidth]{plat0_88.pdf}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
  \node at (0.1, 1.05) {(b)};
\end{scope}
\end{tikzpicture}
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[clip=true, trim=105 40 105 20, width=0.3\linewidth]{mw0_88.pdf}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
  \node at (0.1, 1.025) {(c)};
\end{scope}
\end{tikzpicture}
\caption{2D wet foam (packing fraction $\phi = 0.88$) as represented in (a) the bubble model, (b) a \textit{Plat} simulation, and (c) the force model described here.}
\end{figure}

\begin{acknowledgments}
%It is a great pleasure to have this opportunity to acknowledge our many interactions with Dominique Langevin and her steadfast contribution to the community of foam research, including the Eufoam conference series.
%We thank F Dunne and J Winkelmann for valuable assistance in preparing this manuscript.
Research is supported in part by a research grant from Science Foundation Ireland (SFI) under grant number 13/IA/1926. We also acknowledge the support of the MPNS COST Action MP1305 ``Flowing'' matter and the European Space Agency ESA MAP Metalfoam (AO-99-075) and Soft Matter Dynamics (contract: 4000115113).
\end{acknowledgments}

%For the \correspondingauthor command to work you need at least an empty
%bibliography block. So if you have no citations add an empty block::
%\begin{thebibliography}{9}
%\end{thebibliography}

\begin{thebibliography}{00}
 \bibitem{Durian95} Durian, D.J., \textit{Foam mechanics at the bubble scale}, Phys. Rev.
 Lett. \textbf{75}, 4780-4783, 1995. 

 \bibitem{Durian97} Durian, D.J., \textit{Bubble-scale model of foam
 mechanics: Melting, nonlinear behavior, and avalanches}, Phys. Rev.
 E \textbf{55}, 1739-1751, 1997. 
 
 \bibitem{Langlois08} Langlois, V.J.,  Hutzler, S. and Weaire, D., \textit{Rheological properties of the
 soft-disk model of two-dimensional foams}, Phys. Rev. E \textbf{78}
 021401, 2008.

\bibitem{MorseWitten} Morse, D.C. and T.A. Witten, \textit{Droplet elasticity
in weakly compressed emulsions}, Europhys. Lett. \textbf{22}, 549-555, 1993.

\bibitem{Bolton91} Bolton, F. and Weaire, D., \textit{The effects of Plateau borders in the two-dimensional soap froth. I. Decoration lemma and diffusion theorem}, Phil. Mag. B, \textbf{63}, 795-809, 1991.

\bibitem{Bolton92} Bolton, F. and Weaire, D., \textit{The effects of Plateau borders in the two-dimensional soap froth. II. General simulation and analysis of rigidity loss transition}, Phil. Mag. B, \textbf{65}, 473-487, 1992.

\bibitem{brakke1992} Brakke, K.A., \textit{The Surface Evolver}, Experiment. Math., \textbf{1}, 141-165, 1992.

\bibitem{HoehlerAddad} H\"ohler R. and Cohen-Addad S., \textit{Many-body
interactions in soft jammed materials}, Soft Matter, \textbf{13}, 1371-1383, 2017.

\bibitem{thispaper} Hutzler, S. and Weaire, D. and H\"ohler, R., \textit{Bubble-bubble interactions in a 2D foam, close to the wet limit}, Adv. Colloid Interface Sci, (submitted, 2017).

\bibitem{Winkelmann16} Winkelmann, J., Dunne, FF.,  Langlois, V.J., 
 M\"obius,M.E., Weaire,D. and  Hutzler,S. 
 \textit{2D foams above the jamming transition: Deformation matters}, 
 Colloids Surf., A, (in press, \href{http://dx.doi.org/10.1016/j.colsurfa.2017.03.058}{DOI:10.1016/j.colsurfa.2017.03.058}, 2016).
\end{thebibliography}

\end{document}

