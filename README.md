# README #

LaTeX Code for all contribution to the 2nd Sir Edwards Symposium in Cambridge (abstract, posters and talks).
https://www.turing-gateway.cam.ac.uk/event/tgmw43

I am proud to announce that the poster from Poster/poster.tex was awarded one of the poster prices at the above mentioned conference.