\include{header}

\title{\Huge {2D foams above the jamming transition:} \\ Deformation matters}
\author{\Large{\underline{J.~Winkelmann}$^{a}$, F.F.~Dunne$^{a}$, V.J. Langlois$^{b}$, M.E.~Möbius$^{a}$, D.~Weaire$^{a}$ and S.~Hutzler$^{a}$}}
\subtitle{
\Large{$^{a}$School of Physics, Trinity College Dublin, The University of Dublin, Ireland\\
$^{b}$Laboratoire de Géologie de Lyon, Université Claude Bernard Lyon 1, France
}}
\date{}

\titlegraphic{%
  \includegraphics[clip=true, trim= 10 10 0 10,width=1.1\linewidth]{Abbildungen/trinity-stacked.jpg}
}

\institute{%
 \includegraphics[width=1.1\linewidth]{Abbildungen/foamarms.png}%
}

\begin{document}

\begin{tcolorbox}[
arc=10mm,
colback=white
]
\centering
\huge{
\bubble{
Simulations of 2D foams above the jamming point suggest a different form for the variation of the average contact number with packing fraction from that in soft disk systems.
}
}
\end{tcolorbox}
\vspace{-0.25cm}

% Model descriptions %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{columns}
% Soft disk model %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{column}{0.52\textwidth}
\vspace{-1cm}
\begin{block}{The soft disk simulation}
\begin{columns}[c]
\column{0.6\textwidth}
\vspace{-1cm}
\centering
\begin{figure}
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\centering
\includegraphics[clip=true, trim=265 195 50 136, scale=3]{Abbildungen/Packing09.pdf}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\draw[-latex, line width=1mm] (0.5, 0.5) -- node[midway, below] {$R_n$} (0.755, 0.765);
\end{scope}
\end{tikzpicture}
\caption{A soft disk with corresponding overlaps.}
\label{overlap}
\end{figure}

\column{0.35\textwidth}
\vspace{-1cm}
\centering
\begin{figure}
\includegraphics[clip=true, trim=85 0 85 0, width=0.7\textwidth]{Abbildungen/SoftDiskSamplePhi0_9.png}
\caption{Soft disk packing at $\phi = 0.90$.}
\end{figure}
\end{columns}

\begin{columns}[T]
\column{0.5\textwidth}
\vspace{-1.3cm}
\begin{itemize}
\item[]
\textbf{Interaction and minimisation:}
\begin{itemize}
\item
\alert{Random} initial configuration at packing fraction $\phi$
\item
Interaction dependents on overlap $\delta_{ij}$:
\vspace{-0.25cm}
\begin{equation}
U_{\text{soft}} = \frac{1}{2} \sum_{i<j}^N \delta_{ij}^2
\end{equation}
\end{itemize}
\end{itemize}
\column{0.5\textwidth}
\begin{itemize}
\item[] ~
\begin{itemize}
\item
\textbf{Energy minimisation: Conjugate Gradient}
\end{itemize}
\item[]
\textbf{\alert{Deformation:}}
\begin{itemize}
\item
Disks do \alert{not deform}
\item
No cell area conservation
\end{itemize}
\end{itemize}
\end{columns}

\end{block}
\end{column}
% PLAT simulation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{column}{0.5\linewidth}%s
\vspace{-1cm}
\begin{block}{The \texttt{Plat} simulation for 2D foam}%
\vspace{-0.5cm}
\begin{columns}[T]
\column{0.6\textwidth}
\centering
\begin{figure}
\captionsetup{format=hang}
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[clip=true, trim=210 395 300 310, scale=3]{Abbildungen/denseMidThin.pdf}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
  \node[white] (pb) at (0.88, 0.565) {$p_b$};
  \node at (0.5, 0.45) {pressure $p_i$};
  \node at (0.33, 0.87) {$p_j$};
  \node[circle, fill, inner sep =1mm, label={[xshift=2cm, yshift=1.5cm]0:{$(x_n, y_n)$}}] (vertex) at (0.67, 0.737) {};
  \draw[latex-, line width=1mm] (vertex) -- (0.90, 0.81);
  \node[circle, fill, inner sep =1mm] at (0.73, 0.683) {};
  \node[] at (0.9, 0.35) {$\gamma$};
  \node[] at (0.8, 0.35) {$\gamma$};
  \node[] at (0.3, 0.1) {$2 \gamma$};
\end{scope}
\end{tikzpicture}
\caption{A deformed bubble in \texttt{Plat} ~\cite{PLAT1, PLAT2}.}
\end{figure}

\column{0.37\textwidth}
\begin{figure}
\includegraphics[clip=True, trim=60 170 60 170, width=0.7\textwidth]{Abbildungen/denseMid}
\caption{\texttt{Plat} foam at $\phi=0.90$.}
\end{figure}
\end{columns}

\vspace{-1cm}
\begin{multicols}{2}
\begin{itemize}
\item[]
\textbf{Variables:}
\begin{itemize}
\item
Vertex coordinates $(x_n, y_n)$
\item
Cell and Plateau border pressures
\end{itemize}

\item[]
\textbf{Constraints for equilibrium:}
\begin{itemize}
\item
Const. cell area
\item
Circular arcs meet tangentially at a vertex $(x_n, y_n)$
\item
\textit{Laplace--Young law} is fulfilled for cells and Plateau borders
\end{itemize}
\item[]
\textbf{\alert{Deformation:}}
\begin{itemize}
\item
Bubbles change shape upon contact
\end{itemize}
\end{itemize}
\end{multicols}
\end{block}%
%\vspace{-0.25cm}
\end{column}
\end{columns}

% Morse--Witten Theory and simulation %%%%%%%%%%%%%%%%%%%%%%%%%
\begin{columns}
\column{0.5\textwidth}
\begin{block}{The Morse--Witten theory for 2D bubbles}
\begin{columns}
\column{0.4\textwidth}
\centering
\begin{figure}
\includegraphics[width=0.83\textwidth]{Abbildungen/bubbleDescr.pdf}
\caption{Deformation of a 2D bubble with point force.}
\end{figure}
\begin{equation}
r(\theta) = R_0 (1 + \delta r(\theta))
\end{equation}
\column{0.6\textwidth}
\centering
\begin{tcolorbox}[
colback=tulight,
boxrule=5pt,
colframe=blue,
arc=5mm,
]
What is the shape $\delta r(\theta)$ of a \textcolor{blue}{single, incompressible 2D bubble}, subject to a single contact force $f$?
\end{tcolorbox}

\begin{tcolorbox}[
colback=white,
boxrule=1pt,
colframe=black,
arc=5mm,
]
\textbf{Linear differential equation} for the $\delta r(\theta)$:
\begin{equation*}
-\left( \frac{\mathrm{d}^2}{\mathrm{d}\theta^2} \right) \delta r(\theta) = A + \frac{f}{\pi} \cos\theta
\end{equation*}
\end{tcolorbox}

\begin{tcolorbox}[
colback=white,
boxrule=1pt,
colframe=black,
arc=5mm,
]
\textbf{Solution} for $\delta r(\theta)$:
\begin{equation*}
\delta r(\theta) = \frac{f}{2 \pi} g(\theta)\,, \quad g(\theta) = (\pi - \theta) \sin\theta - \frac{\cos\theta}{2} - 1
\end{equation*}
\begin{itemize}
\item
$\delta r$ \textcolor{blue}{linear} in $f$ \Rightarrow~ {} superposition for multiple $f$
\end{itemize}
\end{tcolorbox}
\end{columns}
\vspace{0.5cm}
\normalsize{
D.~Weaire, R.~Höhler, and S.~Hutzler. \emph{Bubble-bubble interactions in a 2d foam, close to the wet limit.}Adv. Colloid Interface Sci, accepted, (2017). \url{https://dx.doi.org/10.1016/j.cis.2017.07.004}.}
\end{block}

\column{0.5\textwidth}
\begin{block}{The Morse--Witten (MW) simulation model}
\begin{columns}[T]
\column{0.55\textwidth}
\centering
\vspace{-0.5cm}
\begin{figure}
\includegraphics[width=0.5\textwidth]{Abbildungen/MW_diagram.pdf}
\caption{Bubble deformation with multiple forces.}
\end{figure}
\column{0.5\textwidth}
\centering
\vspace{-0.5cm}
\begin{figure}
\includegraphics[width=0.5\textwidth, clip=true, trim=120 40 105 40]{Abbildungen/MWSamplePhi0_9.png}
\caption{MW foam at $\phi = 0.90$.}
\end{figure}
\end{columns}

\begin{columns}
\column{0.5\textwidth}
\vspace{-2.6cm}
\begin{tcolorbox}[
colback=white,
boxrule=5pt,
colframe=black,
arc=5mm,
]
\textbf{Shape} for multiple forces:
\begin{equation*}
\delta r(\theta) = \frac{1}{2 \pi} \sum_{c} f_c g(\theta_c - \theta)
\end{equation*}
\end{tcolorbox}
\vspace{-0.27cm}
\begin{itemize}
\item[] \textbf{Equilibration procedure}
\begin{enumerate}
\item
Identify contacts, update $\delta r$
\item
Calc. forces from $\delta r$, inverting Eq.
\item
Balance forces at contacts
\end{enumerate}
\end{itemize}

\column{0.5\textwidth}
\vspace{-1.9cm}
\begin{itemize}
\item[]~
\begin{itemize}
\item[4.]
Move bubble centers in force direction
\item[5.]
Recalculate $\delta r$ with Eq.
\end{itemize}
\end{itemize}
\begin{itemize}
\item[] \textbf{Constraints for equilibrium}
\begin{itemize}
\item
$\delta r(\theta)$ consistent with $f_i$
\item
force balance at films
\item
force balance on each bubble
\end{itemize}
\end{itemize}
\end{columns}
\end{block}
\end{columns}

% Z(phi) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{alertblock}{The average contact number with increasing packing fraction $Z(\phi)$}
\begin{columns}[T]
\column{0.3\textwidth}
\centering
\textbf{Soft disk simulation:}
\vspace{0.2cm}
\begin{itemize}
\item
\textcolor{blue}{$Z - Z_c \propto \sqrt{\phi - \phi_c}$}
\item
Well known behaviour in soft disk systems \cite{silbert_2002, ohern_2003, vanhecke_2010}
\item
Square-root scaling also observed in experimental data for photo-elastic disks \cite{majmudar_2007}
\item
Power law relationship $Z(\phi)$ for a confined bubble raft experiment~\cite{katgert_2010}, \textit{however}:
\begin{itemize}
\item
Identification of contacting bubbles?
\item
Definition of liquid fraction?
\end{itemize}
\end{itemize}

\column{0.3\textwidth}
\vspace{-0.5cm}
\begin{figure}
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[width=0.83\textwidth]{Abbildungen/MeanZPLAT.pdf}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\draw[latex-latex, line width=4.] (0.25, 1.01) node[left] {\large Wet} -- (0.85, 1.01)  node[right] {\large Dry};
\node at (0.06, 0.15) {\small{$Z_c$}};
\draw[-, line width=0.7] (0.08, 0.1526) -- (0.16, 0.1526);
\end{scope}
\end{tikzpicture}
%\caption{Average $Z(\phi)$ for different simulation models.}
\end{figure}

\column{0.4\textwidth}
\centering
\textbf{\texttt{Plat} simulation:}
\vspace{0.1cm}
\begin{itemize}
\item
\textcolor{red}{$Z - Z_c \propto \phi - \phi_c$}
\item
Periodic system: $Z_c = 4 (1 - \nicefrac{1}{N})$
\item
data averaged over 10,000 simulations of $N = 60$ bubble systems
\item
hints of this in earlier simulations (\texttt{Plat} \cite{PLAT1} and lattice~gas~model~\cite{sun_2004})
\end{itemize}
\vspace{0.4cm}
\textbf{Morse--Witten (MW) model:}
\vspace{0.1cm}
\begin{itemize}
\item
Results are \textit{preliminary}
\item
\textcolor{purple}{$Z - Z_c \approx \phi - \phi_c$}
\item
data averaged over 5 simulations of $N = 100$ bubble systems
\end{itemize}
\end{columns}
%\normalsize{
%J. Winkelmann, F.F.~Dunne, V.J.~Langlois, M.E.~Möbius, D.~Weaire and S. Hutzler.
%\emph{2D foams above the jamming transition: Deformation matters},
%Colloids Surf. A, in press, (2017).
%\url{https://doi.org/10.1016/j.colsurfa.2017.03.058}.
%}
\vspace{-0.5cm}
\end{alertblock}
\vspace{-0.25cm}

% Distribution of separation between bubbles %%%%%%%%%%%%%%%%%%%%%%
\begin{block}{The separation between bubbles at the critical packing fraction}
\begin{columns}[T]
\column{0.25\textwidth}
\begin{figure}
\captionsetup{format=hang}
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[width=0.67\linewidth]{Abbildungen/Separation0_845.pdf}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\node at (0.65, 0.75) {\Huge \textcolor{red}{$w_i$}};
\end{scope}
\end{tikzpicture}
\caption{Separations $w_i$ between different disks/bubbles.}
\end{figure}
\column{0.455\textwidth}
\vspace{0.5cm}
 \Large{
\textbf{Is $Z(\phi)$ determined by the distribution separation between bubbles?}
%\begin{equation}
%\frac{\partial Z}{\partial \phi} = \frac{\partial \varepsilon}{\partial \phi}\frac{\partial Z}{\partial \varepsilon} = \frac{1}{2 \phi_c} \frac{\partial Z}{\partial \epsilon} \,, \qquad \text{compression: } \epsilon = \frac{\phi - \phi_c}{2 \phi_c}
%\end{equation}
\vspace{1cm}
\begin{tcolorbox}[
colback=tulight,
arc=5mm,
]
\textit{Crude argument} \cite{siemens_2010a}: For affine compressions at $\phi_c$, $Z(\phi) - Z_c$ is given by  the radial integral over the distribution of separation $f(w)$ in the limit $\epsilon \approx \nicefrac{w}{D} \rightarrow 0$
\begin{equation}
Z(\phi) - Z_c = 2 \pi \int_0^{\epsilon D} \dx w f(w) (w + D)\,, \qquad \text{compression: } \epsilon = \frac{\phi - \phi_c}{2 \phi_c}
\end{equation}
\small{
$\phi_c$: critical packing fraction.
$D$: average diameter.
}
\end{tcolorbox}
}
\begin{columns}[T]
\column{0.52 \linewidth}
\begin{tcolorbox}[
colback=tulight,
boxrule=5pt,
colframe=red,
arc=5mm,
]
\centering
\textbf{Soft disks:}
\begin{equation}
f(w) \propto \left(\frac{w}{D}\right)^{-\frac{1}{2}} \Rightarrow \textcolor{red}{Z - Z_c \propto \sqrt{\phi - \phi_c}}
\end{equation}
\end{tcolorbox}
\column{0.475 \linewidth}
\begin{tcolorbox}[
colback=tulight,
boxrule=5pt,
colframe=red,
arc=5mm,
]
\centering
\textbf{2D foams:}
\begin{equation}
f(w) = \text{const.} \Rightarrow \textcolor{red}{Z - Z_c \propto \phi - \phi_c}
\end{equation}
\end{tcolorbox}
\end{columns}
\column{0.3\textwidth}
\begin{figure}
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[width=0.83\linewidth]{Abbildungen/FritzAnalysisZ}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
  \node[text width=6cm] (foams) at (0.5, 0.27) {\small{finite limiting value for foams}};
  \node[text width=5cm] (softdisks) at (0.55, 0.85) {\small{divergence for soft disks}};
  \draw[-latex, line width=1mm] (softdisks) -- (0.23, 0.85);
  \draw[-latex, line width=1mm] (foams) -- (0.5, 0.56);
\end{scope}
\end{tikzpicture}
\caption{
Distribution of separation.
}
\label{f:distributionpbwdith2}
\end{figure}
\end{columns}
\vspace{-0.5cm}
\end{block}

\vspace{-1cm}
%%%%%%%%%%%%%%%%%% REFERENCES %%%%%%%%%%%%%%%%%%%%%%%%
  \vspace*{\fill}
  \begin{columns}[b]
  \column{0.55\textwidth}
  \begin{block}[fonttitle=\normalsize, arc=10mm]{References}
    \begin{multicols}{3}
      \nocite{*}\footnotesize%
      \printbibliography%
    \end{multicols}
  \end{block}
  \vspace{-1.25cm}
  \column{0.45\textwidth}
  \centering
  \begin{columns}
  \column{0.5\linewidth}
  \includegraphics[width=\linewidth]{Abbildungen/sfi_logo}
  \column{0.5\linewidth}
  \includegraphics[width=\linewidth, clip=true, trim=325 0 0 0]{Abbildungen/Longlogo.jpg}
  \end{columns}
\end{columns}  

\end{document}
